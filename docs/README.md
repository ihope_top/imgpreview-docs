---
home: true
heroImage: /img/logo.png
heroText: hevue-img-preview
tagline: 基于Vue.js的可能很好用的网页端图片浏览插件
actionText: 快速上手 →
actionLink: /zh/guide/getting-started
features: null
footer: MIT Licensed | Copyright © 2021 贺永胜
---
